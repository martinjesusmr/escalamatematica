//FUNCION PARA CREAR LAS SESIONES O SEGMENTOS CORRESPONDIENTES A LA TABLA
function crearSesion(respuesta, titulo){
      let sesion = "";

      sesion+= 
      "<tr>" +
            "<th colspan='3'>" + titulo + "</th>" +                                             
      "<tr>"
       
       sesion += 
       "<tr>"+
            "<th> Dato1 </th>"+
            "<th> Dato2 </th>"+
            "<th> Resultado</th>"+
       "</tr>";
       
       
       sesion+=respuesta;

       return sesion;
}


function crearEscala(){
    
      let numero=document.querySelector("#entrada").value;

      let rta=crearOperacion(0,numero);
      let rta2=crearOperacion(1,rta[0]);
      
      let tabla="<table class='center'>";

      //AGREGO EL TITULO PRINCIPAL DE LA TABLA
      tabla+= 
      "<tr>" +
             "<th colspan='3'>" + "ESCALA DEL NUMERO: " + numero + "</th>" 
      "<tr>";

      //AGREGO LA SESIÓN DE MULTIPLICAR EN LA TABLA
      tabla += crearSesion(rta[1], "OPERACIÓN MULTIPLICAR");    
      //AGREGO LA SESIÓN DE DIVIDOR EN LA TABLA
      tabla += crearSesion(rta2[1], "OPERACIÓN DIVIDIR");     

      tabla+="</table>";

      
      document.querySelector("#rta3").innerHTML=tabla;
}
   

function crearOperacion(operacion,numero){

    let vectorRta=new Array(2);
    let rta="";
     for(let i=1;i<=10;i++)
      {
      	
            let operacionResultado=0;
            if(operacion==0)
                  operacionResultado=numero*i;
            else
                  operacionResultado=numero/i;

            let mensaje=numero;
            numero=operacionResultado;
            rta+="\n<tr>";
            rta+="\n<td>"+mensaje+"</td>";
            rta+="\n<td>"+i+"</td>";
            rta+="\n<td>"+operacionResultado+"</td>";
            rta+="\n</tr>";
      }
      vectorRta[0]=numero;
      vectorRta[1]=rta;
      return vectorRta;
}

  