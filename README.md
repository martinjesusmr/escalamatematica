![Escala](http://www.madarme.co/portada-web.png)

# Escala numerica 📏

Esto es un proyecto de programación web donde se realiza un algoritmo donde se calcula la escala numerica de un numero ingresado.

## Tabla de contenido:

1. [Escala númerica](#escala-numerica-)
2. [Caracteristicas](#caracteristicas)
3. [Autor](#autor)

## Caracteristicas:

Proyecto que recibe un dato del usuario

## Ejemplo:

Esto es un ejemplo de los resultados obtenidos por la escala numerica

| Dato 1 | Dato 2  | 
|--------|---------|
| 0      | 3       |     
| 1      | 2       |   

## Tecnologias:

* Javascript
* CSS
* HTML

## Autor: 

Nombre: [Martin de Jesus Medina Ruvian](https://martinmedinaruvian.github.io/index/)
#### Código: 1151791
